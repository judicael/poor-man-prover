
type var = BVar of int

type fun_symb = string

type rel_symb = string

type term =
  | Var of var
  | App of fun_symb * term list
  | Iota of string * prop

and atom  = rel_symb * term list

and prop =
  | Atom of atom
  | Conj of prop * prop
  | Disj of prop * prop
  | Impl of prop * prop
  | Equiv of prop * prop
  | Neg of prop
  | Forall of string * prop (* string is for pretty-printing only *)
  | Exists of string * prop (* here too *)

type env = string list

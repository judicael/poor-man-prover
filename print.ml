open Terms

module List = struct
  include List
  let iter3 f l m n =
    iter2 (fun a (b,c) -> f a b c) l (combine m n)
  let map3 f l m n =
    map2 (fun a (b,c) -> f a b c) l (combine m n)
  let iteri2 f l m =
    iteri (fun i (a,b) -> f i a b) (combine l m)
  let mapi2 f l m =
    mapi (fun i (a,b) -> f i a b) (combine l m)
  let rec fold_map f e l =
    match l with
    | [] -> e, []
    | x :: t -> let e', y = f e x in
      let e'', t' = fold_map f e' t in
      e'', y :: t'
  let fold_mapi f e l =
    let rec aux i e l =
      match l with
      | [] -> e, []
      | x :: t -> let e', y = f i e x in
        let e'', t' = aux (i + 1) e' t in
        e'', y :: t'
    in aux 0 e l
  let fold_lefti f e l =
    snd (fold_left (fun (i, a) b -> (i + 1, f i a b)) (0, e) l)
  let range a b =
    let rec aux a b r =
      if a >= b then r
      else aux a (b - 1) ((b - 1) :: r)
    in aux a b []
end

module Int = struct type t = int let compare = compare end
module ISet = Set.Make(Int)
module IMap = Map.Make(Int)
module SSet = Set.Make(String)

let ( <|> ) a b =
  let rec aux a b r =
    if a >= b then r else aux a (b - 1) ((b - 1) :: r)
  in aux a b []

type env = SSet.t * string list

let push v (sym, vars) =
  let rec aux x d =
    if SSet.mem x sym then
      aux (v ^ string_of_int d) (d + 1)
    else
      (SSet.add x sym, x :: vars)
  in
  aux v 1

let nvar (sym, vars) n = List.nth vars n
let hd env = nvar env 0

let rec symbols_t = function
  | Var _ -> SSet.empty
  | App (f, l) -> List.fold_left SSet.union (SSet.singleton f) (List.map symbols_t l)
  | Iota (_, f) -> symbols_f f

and symbols_f = function
  | Atom (f, l) -> List.fold_left SSet.union (SSet.singleton f) (List.map symbols_t l)
  | Conj (f1, f2) | Disj (f1, f2) | Impl (f1, f2) | Equiv (f1, f2) ->
    SSet.union (symbols_f f1) (symbols_f f2)
  | Neg f1 | Forall (_, f1) | Exists (_, f1) -> symbols_f f1

let make_env f = (symbols_f f, [])

let fprint_var env ff v = match v with
  | BVar k -> Format.fprintf ff "%s" (nvar env k)

let fprint_constant_string s ff () = Format.fprintf ff "%s" s
let fprint_comma = fprint_constant_string ", "

let rec fprint_term env ff t = match t with
  | Var v -> fprint_var env ff v
  | App (f, []) -> Format.fprintf ff "%s" f
  | App (f, l) -> Format.fprintf ff "%s(%a)" f
                    (Format.pp_print_list
                       ~pp_sep:fprint_comma
                       (fprint_term env))
                    l
  | Iota (x, f) ->
    let nenv = push x env in
    let x = hd nenv in
    Format.fprintf ff "ι[%s]: (%a)" x (fprint_prop nenv) f

and fprint_prop env ff f = match f with
  | Atom ("=", [x1; x2]) ->
    Format.fprintf ff "%a = %a" (fprint_term env) x1 (fprint_term env) x2
  | Atom (f, []) -> Format.fprintf ff "%s" f
  | Atom (f, l) -> Format.fprintf ff "%s(%a)" f
                     (Format.pp_print_list
                        ~pp_sep:fprint_comma (fprint_term env)
                     ) l
  | Conj (f1, f2) -> Format.fprintf ff "(%a) & (%a)"
                       (fprint_prop env) f1
                       (fprint_prop env) f2
  | Disj (f1, f2) -> Format.fprintf ff "(%a) | (%a)"
                       (fprint_prop env) f1
                       (fprint_prop env) f2
  | Impl (f1, f2) -> Format.fprintf ff "(%a) => (%a)"
                       (fprint_prop env) f1
                       (fprint_prop env) f2
  | Equiv (f1, f2) -> Format.fprintf ff "(%a) <=> (%a)"
                        (fprint_prop env) f1
                        (fprint_prop env) f2
  | Neg f1 -> Format.fprintf ff "~(%a)" (fprint_prop env) f1
  | Forall (x, f1) ->
    let nenv = push x env in
    let x = hd nenv in
    Format.fprintf ff "![%s]: (%a)" x (fprint_prop nenv) f1
  | Exists (x, f1) ->
    let nenv = push x env in
    let x = hd nenv in
    Format.fprintf ff "?[%s]: (%a)" x (fprint_prop nenv) f1

type 'a catenable_list = | Elem of 'a | Cat of 'a catenable_list list

let empty_cl = Cat []
let cat l1 l2 = Cat [l1; l2]
let cons x l = cat (Elem x) l

let cat_list l =
  let rec aux l r =
    match l with
    | Elem x -> x :: r
    | Cat l -> List.fold_right aux l r
  in aux l []

let new_name =
  let cnt = ref 0 in
  fun prefix ->
    incr cnt;
    prefix ^ "_" ^ string_of_int !cnt


let rec term_vars1 depth s t =
  match t with
  | Var (BVar n) ->
    if n >= depth then
      ISet.add (n - depth) s
    else
      s
  | App (f, l) ->
    List.fold_left (term_vars1 depth) s l
  | Iota (_, f) -> prop_vars1 (depth + 1) s f

and prop_vars1 depth s f =
  match f with
  | Atom (f, l) -> List.fold_left (term_vars1 depth) s l
  | Conj (f1, f2) | Disj (f1, f2) | Impl (f1, f2) | Equiv (f1, f2) ->
    prop_vars1 depth (prop_vars1 depth s f2) f1
  | Neg f1 -> prop_vars1 depth s f1
  | Forall (_, f1) | Exists (_, f1) -> prop_vars1 (depth + 1) s f1

let term_vars = term_vars1 0 ISet.empty
let prop_vars = prop_vars1 0 ISet.empty

let rec add_foralls l f =
  match l with
  | [] -> f
  | v :: t -> add_foralls t (Forall (v, f))

let rec translate_vars_t mp depth t = match t with
  | Var (BVar v) ->
    if v >= depth then
      Var (BVar ((IMap.find (v - depth) mp) + depth))
    else
      t
  | App (f, l) -> App (f, List.map (translate_vars_t mp depth) l)
  | Iota (x, f) -> Iota (x, translate_vars_f mp (depth + 1) f)

and translate_vars_f mp depth f = match f with
  | Atom (f, l) ->
    Atom (f, List.map (translate_vars_t mp depth) l)
  | Conj (f1, f2) ->
    Conj (translate_vars_f mp depth f1, translate_vars_f mp depth f2)
  | Disj (f1, f2) ->
    Disj (translate_vars_f mp depth f1, translate_vars_f mp depth f2)
  | Impl (f1, f2) ->
    Impl (translate_vars_f mp depth f1, translate_vars_f mp depth f2)
  | Equiv (f1, f2) ->
    Equiv (translate_vars_f mp depth f1, translate_vars_f mp depth f2)
  | Neg f1 ->
    Neg (translate_vars_f mp depth f1)
  | Forall (x, f1) ->
    Forall (x, translate_vars_f mp (depth + 1) f1)
  | Exists (x, f1) ->
    Exists (x, translate_vars_f mp (depth + 1) f1)

let rec substn_t sub depth t = match t with
  | Var (BVar v) ->
    if v >= depth then sub.(v - depth) else t
  | App (f, l) -> App (f, List.map (substn_t sub depth) l)
  | Iota (x, f) -> Iota (x, substn_f sub (depth + 1) f)

and substn_f sub depth f = match f with
  | Atom (f, l) ->
    Atom (f, List.map (substn_t sub depth) l)
  | Conj (f1, f2) ->
    Conj (substn_f sub depth f1, substn_f sub depth f2)
  | Disj (f1, f2) ->
    Disj (substn_f sub depth f1, substn_f sub depth f2)
  | Impl (f1, f2) ->
    Impl (substn_f sub depth f1, substn_f sub depth f2)
  | Equiv (f1, f2) ->
    Equiv (substn_f sub depth f1, substn_f sub depth f2)
  | Neg f1 ->
    Neg (substn_f sub depth f1)
  | Forall (x, f1) ->
    Forall (x, substn_f sub (depth + 1) f1)
  | Exists (x, f1) ->
    Exists (x, substn_f sub (depth + 1) f1)


let rec translate_prop env f = match f with
  | Atom (("=" | "$true" | "$false" as f), l) ->
    let nl = List.map (translate_term env) l in
    (Atom (f, List.map fst nl), Cat (List.map snd nl))
  | Atom (f, l) ->
    let nl = List.map (translate_term env) l in
    (Atom ("P_" ^ f, List.map fst nl), Cat (List.map snd nl))
  | Conj (f1, f2) ->
    let nf1, r1 = translate_prop env f1 in
    let nf2, r2 = translate_prop env f2 in
    Conj (nf1, nf2), cat r1 r2
  | Disj (f1, f2) ->
    let nf1, r1 = translate_prop env f1 in
    let nf2, r2 = translate_prop env f2 in
    Disj (nf1, nf2), cat r1 r2
  | Impl (f1, f2) ->
    let nf1, r1 = translate_prop env f1 in
    let nf2, r2 = translate_prop env f2 in
    Impl (nf1, nf2), cat r1 r2
  | Equiv (f1, f2) ->
    let nf1, r1 = translate_prop env f1 in
    let nf2, r2 = translate_prop env f2 in
    Equiv (nf1, nf2), cat r1 r2
  | Neg f1 ->
    let nf1, r1 = translate_prop env f1 in
    Neg nf1, r1
  | Forall (x, f1) ->
    let nf1, r1 = translate_prop (push x env) f1 in
    Forall (x, nf1), r1
  | Exists (x, f1) ->
    let nf1, r1 = translate_prop (push x env) f1 in
    Exists (x, nf1), r1

and translate_term env t = match t with
  | Var v -> (t, empty_cl)
  | App (f, l) ->
    let nl = List.map (translate_term env) l in
    (App ("F_" ^ f, List.map fst nl), Cat (List.map snd nl))
  | Iota (x, f) ->
    (* Ensure the bound variable is always among the free variables*)
    let nf, r = translate_prop (push x env) f in
    let fv = ISet.elements (ISet.add 0 (prop_vars nf)) in
    let n = List.length fv in
    let args = List.map ((-) 1) (List.tl fv) in
    let mp = List.fold_lefti (fun i mp v -> IMap.add v i mp) IMap.empty fv in
    let f_name = new_name "I" in
    let p_name = new_name "J" in
    let make_call l =
      Atom (p_name, List.map (fun v -> Var (BVar v)) l)
    in
    let nt = App (f_name, List.map (fun v -> Var (BVar v)) args) in
    let free_names = List.map (nvar env) args in
    let exists = Exists (x, make_call (0 <|> n)) in
    let unique = Forall (x ^ "_1", Forall (x ^ "_2",
      Impl (Conj (make_call (0 :: (2 <|> (n + 1))), make_call (1 <|> (n + 1))),
            Atom ("=", [Var (BVar 1); Var (BVar 0)]))
    ))
    in
    let ax = add_foralls free_names (Impl (Conj (exists, unique), Forall (x,
      Equiv (make_call (0 <|> n),
        Atom ("=", [Var (BVar 0);
        App (f_name, List.mapi (fun i _ -> Var (BVar (i + 1))) args)]))))) in
    let p_ax = add_foralls free_names (Forall (x,
      Equiv (make_call (0 <|> n), translate_vars_f mp 0 nf)
    )) in
    (nt, cons p_ax (cons ax r))

let t = (Forall ("x", Atom ("g", [Iota ("y", Atom ("f", [Var (BVar 0); Var (BVar 1)])); Var (BVar 0)])))

let print_prop_f ff f = fprint_prop (make_env f) ff f

let () = Format.printf "%a@." print_prop_f t
let () = Format.printf "@."
let (r, ax) = translate_prop (make_env t) t
let ax = cat_list ax
let () = Format.printf "%a@." print_prop_f r
let () = List.iter (Format.printf "%a@." print_prop_f) ax

type proof_step =
  | Assertion of prop * proof_sketch
  | SAP of string list * prop * prop * proof_sketch
  | Case of prop * proof_sketch

and proof_sketch = proof_step list

type sequent = prop list * prop list

let print_sequent ff (a, b) =
  Format.fprintf ff "@.";
  List.iter (Format.fprintf ff "%a@." print_prop_f) a;
  Format.fprintf ff "---------------------------------------@.";
  List.iter (Format.fprintf ff "%a@." print_prop_f) b;
  Format.fprintf ff "@."

let rec conj_list l =
  match l with
  | [] -> Atom ("$true", [])
  | [x] -> x
  | x :: t -> Conj (x, conj_list t)

let rec proof_step_substn sub depth s =
  match s with
  | Assertion (p, l) -> Assertion (substn_f sub depth p, proof_sketch_substn sub depth l)
  | SAP (vars, p1, p2, l) ->
    let n = List.length vars in
    SAP (vars, substn_f sub (depth + n) p1, substn_f sub (depth + n) p2,
         proof_sketch_substn sub depth l)
  | Case (p, l) -> Case (substn_f sub depth p, proof_sketch_substn sub depth l)

and proof_sketch_substn sub depth = List.map (proof_step_substn sub depth)

let rec formulas_to_prove hyp obj l =
  match l with
  | [] -> [hyp, [obj]] (* [Impl (conj_list hyp, obj)] *)
  | (SAP (vars, h1, obj1, l1)) :: t ->
    let nh = add_foralls (List.rev vars) (Impl (h1, obj1)) in
    let nnames = List.map (fun v -> App (new_name v, [])) (List.rev vars) in
    let sub = Array.of_list nnames in
    let nh1 = substn_f sub 0 h1 in
    let nobj1 = substn_f sub 0 obj1 in
    let nt = proof_sketch_substn sub 0 t in
    (formulas_to_prove (nh :: hyp) obj l1) @
    (formulas_to_prove (nh1 :: hyp) nobj1 nt)
  | (Assertion (f1, l1)) :: t ->
    (formulas_to_prove hyp f1 l1) @ (formulas_to_prove (f1 :: hyp) obj t)
  | (Case (f1, l1)) :: t ->
    (formulas_to_prove (f1 :: hyp) obj l1) @ (formulas_to_prove ((Neg f1) :: hyp) obj t)

let t = Forall ("x", Forall ("y", Impl (Atom ("f", [Var (BVar 0); Var (BVar 1)]), Atom ("f", [Var (BVar 1); Var (BVar 0)]))))

let sk = [SAP (["x"; "y"], Atom ("f", [Var (BVar 0); Var (BVar 1)]), Atom ("f", [Var (BVar 1); Var (BVar 0)]), []);
          Assertion (Atom ("g", [Var (BVar 0); Var (BVar 1)]), [
            Case (Atom ("h", [Var (BVar 0)]), [])
            ])]

let ff = formulas_to_prove [Atom("w", [])] t sk

let () = Format.printf "@."
let () = List.iter (Format.printf "%a@." print_sequent) ff

